#ifndef DRIVE_H
#define DRIVE_H
#include <iostream>
#include "Bitmap.h"

using namespace std;
class Drive
{
    public:
        Bitmap* bitmap = new Bitmap();
        Bitmap* directory = new Bitmap();
        int numBlocks;
        int blocksBitmap;
        int blocksDirmap;
        string driveName;
        Drive();
        virtual ~Drive();
        void createPartition(string deviceName, double size, string sizeType);
        void mountPartition(string deviceName);
        void unmount();
        void createNewFile(string filename);
        void listFiles();
		void saveBitmaps();
		void deleteBlock(string deviceName);
		void rename(string oldFilename, string newFilename);
		void deleteFile(string filename);
		void copyToFS(string sourcePath, string filename);
		void copyFromFS(string filename, string targetPath);

    protected:

    private:
		int searchFile(string filename);
};

#endif // DRIVE_H
