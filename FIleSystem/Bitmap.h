#ifndef BITMAP_H
#define BITMAP_H
#include <vector>

using namespace std;
class Bitmap
{
    public:
        vector<char> bitmap;
        Bitmap();
        virtual ~Bitmap();
        int getFreeBit();
        void setUsedBit(int pos);
		void setFreeBit(int pos);
        void setBitmap(vector<char> newBitmap);
        vector<int> getUsedBits();
		void printMap();

    protected:

    private:
};

#endif // BITMAP_H
