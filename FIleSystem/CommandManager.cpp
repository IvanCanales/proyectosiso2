#include "stdafx.h"
#include "CommandManager.h"
#include <string>

using namespace std;

CommandManager::CommandManager()
{
}

void CommandManager::execCommand(string command)
{
	string commandType = splitCommand(command);
	
	if (commandType == "create_block") createBlock(command);
	if (commandType == "delete_block") deleteBlock(command);
	if (commandType == "mount") mount(command);
	if (commandType == "unmount") unmount(command);
	if (commandType == "copy_from_fs") copyFromFS(command);
	if (commandType == "copy_to_fs") copyToFS(command);
	if (commandType == "empty")	empty(command);
	if (commandType == "delete") _delete(command);
	if (commandType  == "rename") _rename(command);
	if (commandType == "ls") ls(command);
	if (commandType == "help") help(command);
}

string CommandManager::splitCommand(string& command)
{
	string delimiter = " ";
	size_t pos = 0;
	string commandType = "";
	pos = command.find(delimiter);
	commandType = command.substr(0, pos);
	command.erase(0, pos + delimiter.length());

	return commandType;
}

void CommandManager::createBlock(string command)
{
	string name = splitCommand(command);
	double disksize = stod(splitCommand(command));
	string sizeType = command;
	drive.createPartition(name, disksize, sizeType);
}

void CommandManager::deleteBlock(string command)
{
	drive.deleteBlock(command);
}

void CommandManager::mount(string command)
{
	drive.mountPartition(command);
}

void CommandManager::unmount(string command)
{
	drive.unmount();
}

void CommandManager::copyFromFS(string command)
{
	string filename = splitCommand(command);
	string targetPath = command;
	drive.copyFromFS(filename, targetPath);
}

void CommandManager::copyToFS(string command)
{
	string sourcePath = splitCommand(command);
	drive.copyToFS(sourcePath, command);
}

void CommandManager::empty(string command)
{
	drive.createNewFile(command);
}

void CommandManager::_delete(string command)
{
	drive.deleteFile(command);
}

void CommandManager::_rename(string command)
{
	string oldFilename = splitCommand(command);
	string newFilename = command;
	drive.rename(oldFilename, newFilename);
}

void CommandManager::ls(string command)
{
	drive.listFiles();
}

void CommandManager::help(string command)
{
	cout << "\ncreate_block <device_name> <tamano MB>: Comando para la creacion de una particion para el Sistema de Archivo" << endl;
	cout << "\ndelete_block <device_name> : Eliminar un block device(particion)" << endl;
	cout << "\nmount <device_name> : Montar un block device(Particion)" << endl;
	cout << "\numount <device_name> : Desmontar un block device(Particion)" << endl;
	cout << "\ncopy_from_fs <source_path_fs> <target_filename_myfs> : Copiar un archivo desde el Sistema de Archivos del Sistema Operativo hacia su Sistema de Archivos" << endl;
	cout << "\ncopy_to_fs <source_filename_myfs> <target_path_fs> : Copiar un archivo desde el su Sistema de Archivos al Sistema de archivos del Sistema Operativo" << endl;
	cout << "\nempty <filename> : Crear un archivo vacio" << endl;
	cout << "\ndelete <filename> : Borrar archivo" << endl;
	cout << "\nrename <old_filename> <new_filename> : Renombrar un archivo" << endl;
	cout << "\nls : Listar archivos" << endl;
}

CommandManager::~CommandManager()
{
}