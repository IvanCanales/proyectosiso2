#include "stdafx.h"
#include "Drive.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>

#define BLOCKSIZE	4096

using namespace std;
Drive::Drive()
{
	//ctor
}

Drive::~Drive()
{
	//dtor
}

void Drive::createPartition(string deviceName, double size, string sizeType) {
	driveName = deviceName;
	double sizeBytes = size * 1024 * 1024;
	if (sizeType == "GB")
		sizeBytes *= 1024;
	numBlocks = sizeBytes / BLOCKSIZE;

	int blocksDirectory = numBlocks / 16;
	int numDirectories = (blocksDirectory*BLOCKSIZE) / 264;
	int sizeDirmap = numDirectories / 8;
	blocksDirmap = sizeDirmap / BLOCKSIZE;
	if (sizeDirmap%BLOCKSIZE != 0) blocksDirmap++;
	vector<char> newDirectory;
	for (int i = 0; i < sizeDirmap; i++)
		newDirectory.push_back(0);
	directory->setBitmap(newDirectory);

	int sizeBitmap = numBlocks / 8;       // bytes
	blocksBitmap = sizeBitmap / BLOCKSIZE;
	if (sizeBitmap%BLOCKSIZE != 0) blocksBitmap++;
	vector<char> newBitmap;
	for (int i = 0; i < sizeBitmap; i++)
		newBitmap.push_back(0);
	bitmap->setBitmap(newBitmap);

	for (int i = 0; i < 1 + blocksBitmap + blocksDirmap + blocksDirectory; i++)
		bitmap->setUsedBit(i);

	ofstream disk;
	disk.open(deviceName.c_str(), ios::out|ios::binary);
	for (double i = 0; i < sizeBytes; i++) {
		disk << '0';
	}
	disk.seekp(0);
	disk.write((char*)&numBlocks, 4);
	disk.seekp(BLOCKSIZE);
	for (int i = 0; i < bitmap->bitmap.size(); i++)
		disk.write((char*)&bitmap->bitmap.at(i), 1);
	disk.seekp(BLOCKSIZE + (blocksBitmap*BLOCKSIZE));
	for (int i = 0; i < directory->bitmap.size(); i++)
		disk.write((char*)&directory->bitmap.at(i), 1);

	disk.close();
}

void Drive::mountPartition(string deviceName) {
	vector<char> newBitmap;
	vector<char> newDirectory;
	driveName = deviceName;
	ifstream disk(deviceName.c_str(), ios::binary);
	if (disk.is_open()) {
		disk.seekg(0);
		disk.read((char*)&numBlocks, 4);

		disk.seekg(BLOCKSIZE);
		int sizeBitmap = numBlocks / 8;
		for (int i = 0; i < sizeBitmap; i++)
			newBitmap.push_back(0);
		for (int i = 0; i < sizeBitmap; i++)
			disk.read((char*)&newBitmap.at(i), 1);
		bitmap->setBitmap(newBitmap);

		blocksBitmap = sizeBitmap / BLOCKSIZE;
		if (sizeBitmap%BLOCKSIZE != 0) blocksBitmap++;
		int blocksDirectory = numBlocks / 16;
		int numDirectories = (blocksDirectory*BLOCKSIZE) / 264;
		int sizeDirmap = numDirectories / 8;
		blocksDirmap = sizeDirmap / BLOCKSIZE;
		if (sizeDirmap%BLOCKSIZE != 0) blocksDirmap++;
		disk.seekg(BLOCKSIZE + (blocksBitmap*BLOCKSIZE));
		for (int i = 0; i < sizeDirmap; i++) {
			newDirectory.push_back(0);
			disk.read((char*)&newDirectory.at(i), 1);
		}
		directory->setBitmap(newDirectory);
		disk.close();
	}
	else
		cout << "Unable to open file" << endl;
}

void Drive::unmount() {
	bitmap = new Bitmap();
	directory = new Bitmap();
	numBlocks = 0;
	blocksBitmap = 0;
	blocksDirmap = 0;
}

void Drive::createNewFile(string filename) {
	int blockPos = bitmap->getFreeBit();
	int dirPos = directory->getFreeBit();

	ofstream disk;
	disk.open(driveName.c_str(), ios::in | ios::out|ios::binary);
	disk.seekp(BLOCKSIZE + (blocksBitmap*BLOCKSIZE) + (blocksDirmap*BLOCKSIZE) + (dirPos * 264));
	for (int i = 0; i < 256; i++) {
		if (i < filename.size())
			disk.write((char*)&filename.at(i), 1);
		else if (i == filename.size())
			disk << '\0';
		else
			disk << '0';
	}
	int fileSize = 1;
	disk.write((char*)&fileSize, 4);
	disk.write((char*)&blockPos, 4);

	bitmap->setUsedBit(blockPos);
	directory->setUsedBit(dirPos);

	disk.close();
	saveBitmaps();
}

void Drive::listFiles() {
	vector<int> files;
	files = directory->getUsedBits();

	ifstream disk(driveName.c_str(), ios::binary);
	if (disk.is_open()) {
		for (int i = 0; i < files.size(); i++) {
			string filename = "";
			disk.seekg(BLOCKSIZE + (blocksBitmap*BLOCKSIZE) + (blocksDirmap*BLOCKSIZE) + (files.at(i) * 264));
			for (int j = 0; j < 256; j++) {
				char newChar;
				disk.read((char*)&newChar, 1);

				if (newChar == '\0')
					break;

				filename += newChar;
			}
			cout << filename << endl;
		}
		disk.close();
	}
	else
		cout << "Unable to open file" << endl;
}

void Drive::saveBitmaps()
{
	int sizeBitmap = numBlocks / 8;
	int blocksDirectory = numBlocks / 16;
	int numDirectories = (blocksDirectory * BLOCKSIZE) / 264;
	int sizeDirmap = numDirectories / 8;

	ofstream disk;
	disk.open(driveName.c_str(), ios::in | ios::out|ios::binary);
	disk.seekp(BLOCKSIZE);
	for (int i = 0; i < sizeBitmap; i++)
		disk.write((char*)&bitmap->bitmap.at(i), 1);
	disk.seekp(BLOCKSIZE + (blocksBitmap * BLOCKSIZE));
	for (int i = 0; i < sizeDirmap; i++)
		disk.write((char*)&directory->bitmap.at(i), 1);

	disk.close();
}

void Drive::deleteBlock(string deviceName)
{
	if (remove(deviceName.c_str()) != 0)
		cout << "File " << deviceName << " not found" << endl;
	else
		cout << "File successfully deleted" << endl;
}

void Drive::rename(string oldFilename, string newFilename)
{
	vector<int> files = directory->getUsedBits();
	int filePos = searchFile(oldFilename);

	if (filePos == -1) {
		cout << "file not found" << endl;
		return;
	}
	
	ofstream disk;
	disk.open(driveName.c_str(), ios::in | ios::out | ios::binary);
	disk.seekp(BLOCKSIZE + (blocksBitmap * BLOCKSIZE) + (blocksDirmap * BLOCKSIZE) + (files.at(filePos) * 264));
	for (int i = 0; i < 256; i++) {
		if (i < newFilename.size())
			disk.write((char*)&newFilename.at(i), 1);
		else if (i == newFilename.size())
			disk << '\0';
		else
			disk << '0';
	}
	disk.close();
}

void Drive::deleteFile(string oldFilename)
{
	vector<int> files = directory->getUsedBits();
	int filePos = searchFile(oldFilename);

	if (filePos == -1) {
		cout << "file not found" << endl;
		return;
	}

	ifstream disk(driveName.c_str(), ios::binary);
	if (disk.is_open()) {
		directory->setFreeBit(filePos);

		disk.seekg(BLOCKSIZE + (blocksBitmap * BLOCKSIZE) + (blocksDirmap * BLOCKSIZE) + (files.at(filePos) * 264) + 256);
		int filesize;
		int blockPos;
		disk.read((char*)&filesize, 4);
		disk.read((char*)&blockPos, 4);
		
		for (int i = 0; i < filesize; i++) {
			if (i % (BLOCKSIZE - 4) == 0) {
				bitmap->setFreeBit(blockPos);
				disk.seekg(blockPos * BLOCKSIZE);
				disk.read((char*)&blockPos, 4);
			}

			char newChar;
			disk.read((char*)&newChar, 1);
		}
		disk.close();
	}

	saveBitmaps();
}

void Drive::copyToFS(string sourcePath, string filename)
{
	string file = "";
	int filesize = -1;
	ifstream reader(sourcePath.c_str(), ios::binary);

	if (reader.is_open()) {
		streampos begin, end;
		begin = reader.tellg();
		reader.seekg(0, ios::end);
		end = reader.tellg();
		filesize = end - begin;
		reader.seekg(0);

		char newChar;
		for (int i = 0; i < filesize; i++) {
			reader.read((char*)&newChar, 1);
			file += newChar;
		}

		reader.close();
	}
	else {
		cout << "Unable to open file" << endl;
		return;
	}

	if (filesize == -1) {
		cout << "file not found" << endl;
		return;
	}

	int blockPos = bitmap->getFreeBit();
	int dirPos = directory->getFreeBit();
		
	ofstream disk;
	disk.open(driveName.c_str(), ios::in | ios::out|ios::binary);

	disk.seekp(BLOCKSIZE + (blocksBitmap * BLOCKSIZE) + (blocksDirmap * BLOCKSIZE) + (dirPos * 264));
	for (int i = 0; i < 256; i++) {
		if (i < filename.size())
			disk.write((char*)&filename.at(i), 1);
		else if (i == filename.size())
			disk << '\0';
		else
			disk << '0';
	}
	disk.write((char*)&filesize, 4);
	disk.write((char*)&blockPos, 4);
	directory->setUsedBit(dirPos);

	// write file
	for (int i = 0; i < filesize; i++) {
		if (i%(BLOCKSIZE - 4)== 0) {
			bitmap->setUsedBit(blockPos);
			disk.seekp(blockPos * BLOCKSIZE);
			blockPos = bitmap->getFreeBit();
			disk.write((char*)&blockPos, 4);
		}

		disk.write((char*)&file.at(i), 1);
	}

	disk.close();
	saveBitmaps();
}

void Drive::copyFromFS(string filename, string targetPath)
{
	vector<int> files = directory->getUsedBits();
	int filePos = searchFile(filename);

	if (filePos == -1) {
		cout << "file not found" << endl;
		return;
	}

	ifstream disk(driveName.c_str(), ios::binary);
	if (disk.is_open()) {
		string file = "";
		disk.seekg(BLOCKSIZE + (blocksBitmap * BLOCKSIZE) + (blocksDirmap * BLOCKSIZE) + (files.at(filePos) * 264) + 256);
		int filesize;
		int blockPos;
		disk.read((char*)&filesize, 4);
		disk.read((char*)&blockPos, 4);
			
		for (int i = 0; i < filesize; i++) {
			if (i % (BLOCKSIZE - 4) == 0) {
				disk.seekg(blockPos * BLOCKSIZE);
				disk.read((char*)&blockPos, 4);
			}

			char newChar;
			disk.read((char*)&newChar, 1);
			file += newChar;
		}


		ofstream newFile;
		newFile.open(targetPath.c_str(), ios::out|ios::app|ios::binary);
		for (int i = 0; i < file.size()-1; i++) {
			newFile.write((char*)&file.at(i),1);
		}

		newFile.close();
		disk.close();
	}
}

int Drive::searchFile(string filename)
{
	vector<int> files;
	files = directory->getUsedBits();
	int filePos = -1;

	ifstream disk(driveName.c_str(), ios::binary);
	if (disk.is_open()) {
		for (int i = 0; i < files.size(); i++) {
			string currentFilename = "";
			disk.seekg(BLOCKSIZE + (blocksBitmap * BLOCKSIZE) + (blocksDirmap * BLOCKSIZE) + (files.at(i) * 264));
			for (int j = 0; j < 256; j++) {
				char newChar;
				disk.read((char*)&newChar, 1);

				if (newChar == '\0')
					break;

				currentFilename += newChar;
			}
			if (currentFilename == filename) {
				filePos = i;
				break;
			}
		}
		disk.close();
	}
	else
		cout << "Unable to open file" << endl;

	return filePos;
}
