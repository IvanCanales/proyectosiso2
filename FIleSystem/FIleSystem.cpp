#include "stdafx.h"
#include <string>
#include "CommandManager.h"

int main()
{
	CommandManager cmdManager;
	string cmd = "";
	while (true) {
		cout << "\nIvan File System> ";
		getline(cin, cmd);

		try {
			cmdManager.execCommand(cmd);
		} catch (exception e) {
			cout << "Not a command" << endl;
		}
	}
    return 0;
}

