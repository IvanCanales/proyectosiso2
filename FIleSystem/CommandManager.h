#pragma once
#include <iostream>
#include "Drive.h"

using namespace std;

class CommandManager
{
public:
	CommandManager();
	Drive drive;

	void execCommand(string command);
	string splitCommand(string& command);
	void createBlock(string command);
	void deleteBlock(string command);
	void mount(string command);
	void unmount(string command);
	void copyFromFS(string command);
	void copyToFS(string command);
	void empty(string command);
	void _delete(string command);
	void _rename(string command);
	void ls(string command);
	void help(string command);

	~CommandManager();
};

