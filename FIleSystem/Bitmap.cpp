#include "stdafx.h"
#include "Bitmap.h"
#include <vector>
#include <bitset>
#include <iostream>

using namespace std;
Bitmap::Bitmap()
{
    //ctor
}

Bitmap::~Bitmap()
{
    //dtor
}

int Bitmap::getFreeBit() {
    unsigned char bit = 128;
    int found = -1;
    for (int i=0; i<bitmap.size(); i++) {
        bit = 128;
        for(int j=0; j<8; j++) {
            int freeBit = bitmap.at(i)&bit;
            if (freeBit == 0) {
                found = (i*8) + j;
                break;
            }
            bit = bit>>1;
        }
        if (found != -1) break;
    }
    return found;
}

void Bitmap::setUsedBit(int pos) {
    unsigned char bit = 128;
    int bytePos = pos%8;
    for (int i=0; i<bytePos; i++) {
        bit = bit >> 1;
    }

    int vectorPos = pos/8;
    bitmap.at(vectorPos) =  bitmap.at(vectorPos) | bit;
}

void Bitmap::setFreeBit(int pos)
{
	int vectorPos = pos / 8;
	unsigned char bit = 127;
	unsigned char firstBit = 128;
	unsigned char firstBitOn = bitmap.at(vectorPos) & bit;

	int bytePos = pos % 8;
	for (int i = 0; i < bytePos; i++) {
		bit = bit >> 1;

		if (firstBitOn != 0 && i == 0)
			bit = bit | firstBit;
	}

	bitmap.at(vectorPos) = bitmap.at(vectorPos) & bit;
}

void Bitmap::setBitmap(vector<char> newBitmap) {
    bitmap = newBitmap;
}
vector<int> Bitmap::getUsedBits() {
    unsigned char bit = 128;
    vector<int> found;
    for (int i=0; i<bitmap.size(); i++) {
        bit = 128;
        for(int j=0; j<8; j++) {
            int comparedBit = bitmap.at(i)&bit;
            if (comparedBit != 0)
                found.push_back((i*8) + j);
            bit = bit>>1;
        }
    }
    return found;
}

void Bitmap::printMap()
{
	for (int i = 0; i < bitmap.size(); i++) {
		bitset<8> bitset(bitmap.at(i));
		cout << bitset << endl;
	}
}
